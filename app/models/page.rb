class Page < ActiveRecord::Base
  before_validation :slugify, :set_last_position

  validates :position, :title, presence: true
  validates :slug, uniqueness: true, presence: true

  scope :active, -> { where(active: true) }

  def slugify
    self.slug ||= self.title.parameterize
  end

  def to_s
    title
  end

  def set_last_position
    self.position = Page.last_position + 1 unless self.position
  end

  def self.last_position
    all_sorted.last.try(:position) || 0
  end

  def self.all_sorted
    all.order(:position).reject {|p| p.home_page?}
  end

  def self.default_page
    where(id: default_page_id).first || first
  end

  # TODO: get this dynamically
  def self.default_page_id
    self.first.id
  end

  # TODO: get this dynamically
  def home_page?
    slug == 'home'
  end
end
