$(function() {
  // menu
  $(document).on("page:load", updateMenuWidth);
  $(document).on("ready", function(){
    setTimeout(function() { updateMenuWidth() }, 200);
  });

  $(document).bind('page:change', function() {
    $('a.fancybox').fancybox({ parent: "body"});
  });

  function updateMenuWidth(){
    $("nav h4").slabText();
  };

  // Galleries
  $("div.flickr_img_wrap a").fancybox({
    openEffect: 'none',
    closeEffect: 'none'
  });
});