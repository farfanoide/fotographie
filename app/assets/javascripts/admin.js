//= require ckeditor/init
//= require Sortable
//= require_self

$(function() {
  var sortableContainer = '.admin-editable.pages',
      sortableForm = '.admin-editable.pages form',
      sortDisabledPages = '.js-sort-disabled';

  $(document).on('click', '#sort-pages', function(){
    $(sortDisabledPages).remove();
    $('.admin-editable.pages span.actions').remove();
    this.remove();
    $(sortableContainer + ' .hidden').removeClass('hidden');
    $(sortableForm).addClass('sorting');
    new Sortable(document.getElementById('js-sortable-container'), {
      draggable: ".sort-page"
    });
  });
});
