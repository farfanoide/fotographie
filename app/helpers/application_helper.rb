module ApplicationHelper

  def show_link(resource)
    link_to 'ver', resource, class: :button
  end

  def edit_link(resource)
    link_to 'editar', edit_polymorphic_path(resource), class: :button
  end

  def destroy_link(resource)
    link_to 'Borrar',
      polymorphic_path(resource),
      method: :delete,
      data: { confirm: "Esta seguro que desea borrar '#{resource.to_s}'?" },
      class: :button
  end

  def back_link(path = :back)
    link_to 'volver', path
  end

  def menu_link(text, path)
    content_tag(:h4) do
      content_tag(:span, class: :slabtext) { link_to text, path }
    end
  end
  # devise helpers
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def app_config
    @config ||= OpenStruct.new (JSON.parse(File.open("#{Rails.application.root}/config/app_config.json").readlines.join(' ')) || {})
  end
end
