class AlbumsController < ApplicationController

  def work
    render :index
  end

  def show
    @photoset_id = params[:id]
    render :'albums/show'
  end

  private

  def flickr_user_id
    @flickr_user_id ||= ENV['FLICKR_USER_ID']
  end

  def get_flickr_photosets
    flickr.photosets.getList(user_id: flickr_user_id, primary_photo_extras: 'url_m')
  end

  def set_albums
    albums = []
    get_flickr_photosets.photoset.each { |photoset| albums << Album.new(photoset.to_hash) }
    albums
  end
end
