class ApplicationController < ActionController::Base
  include ApplicationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_pages, :set_social_links
  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u|
      u.permit(:password, :password_confirmation, :current_password)
    }
  end
  add_flash_types :error

  def work

    render 'albums/index'
  end

  private

  def set_pages
    @pages ||= Page.where(active: true)
  end

  def set_social_links
    @social_links ||= app_config.social.map do |link|
      OpenStruct.new({network: link['network'], href: link['url']})
    end
  end
end
