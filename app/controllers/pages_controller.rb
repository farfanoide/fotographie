class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :edit, :update, :destroy, :new, :sort]

  def index
    @pages = Page.all.order(:position)
  end

  def default_page
    @page = Page.default_page
    render 'show'
  end

  def show
  end

  def new
    @page = Page.new
  end

  def edit
  end

  def create
    @page = Page.new(page_params)

    if @page.save
      redirect_to @page, notice: 'La pagina se ha creado correctamente.'
    else
      render :new
    end
  end

  def update
    if @page.update(page_params)
      redirect_to @page, notice: 'La pagina ha sido correctamente actualizada.'
    else
      render :edit
    end
  end

  def destroy
    @page.destroy
    redirect_to pages_url, notice: 'La pagina ha sido borrada.'
  end

  def sort
    @pages = pages_to_sort
    render :index
  end

  def apply_sort
    sort_pages(params[:pages]) if params[:pages]
    @pages = pages_to_sort
    redirect_to pages_path
  end

  private
    def sort_pages(page_ids)
      page_ids.each_with_index { |id, index| Page.find(id).update!(position: index + 1) }
    end

    def set_page
      @page = Page.find(params[:id])
    end

    def page_params
      params.require(:page).permit(:title, :slug, :active, :position, :body)
    end

    def pages_to_sort
      Page.active.order(:position).reject {|p| p.slug == 'home'}
    end
end
