class ContactController < ApplicationController
  def index
  end

  def deliver
    begin
      configure_pony
      Pony.mail(
        from: "#{contact_params['name']}<#{contact_params['mail']}>",
        to: ENV['YEAH_ADMIN_MAIL'],
        headers: { 'Content-Type' => 'text/html' },
        subject: "#{contact_params['last-name']}, #{contact_params['name']} le mando un mail desde YeahEspacio.com.ar",
        body: message_body
      )
      flash[:notice] = 'Mensaje enviado correctamente.'
    rescue
      flash[:error] = 'Ocurrio un problema enviando su mail, por favor intente nuevamente mas tarde.'
    end
    render 'thanks'
  end

  private
  def contact_params
    params.require(:contact).permit(:name, :'last-name', :message, :mail)
  end

  def configure_pony
    Pony.options = {
      via: :smtp,
      via_options: {
        address: 'smtp.gmail.com',
        port: '587',
        user_name: ENV['YEAH_ADMIN_MAIL'],
        password: ENV['YEAH_MAIL_PASS'],
        authentication: :plain,
        enable_starttls_auto: true,
        domain: ENV['HOST']}
    }
  end

  def message_body
    "<html lang='en'><head> <meta charset='UTF-8'>
    <title>YeahEspacio!</title>
  </head> <body>
    <h1>#{contact_params['last-name']}, #{contact_params['name']}le mando un mail desde su pagina:</h1>
    <p> <span>Mensaje: </span> #{contact_params['message']}</p>
    <p> E-mail:
      <a href='mailto:#{contact_params['mail']}'>#{contact_params['mail']}</a>
    </p>
</body></html>"
  end
end
