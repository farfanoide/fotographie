class ApplicationMailer < ActionMailer::Base
  default from: ENV['YEAH_ADMIN_MAIL'], to: ENV['YEAH_ADMIN_MAIL']

  layout 'mailer'
end
