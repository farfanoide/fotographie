class ContactMailer < ApplicationMailer
  def message(contact_params)
    @contat_params = contact_params
    mail(
      subject: "#{contact_params['last-name']}, #{contact_params['name']} le mando un mail desde YeahEspacio.com.ar",
    )
  end
end
