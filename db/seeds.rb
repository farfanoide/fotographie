home_body = <<-eos
<p>Yeah! Es un espacio donde se unen varias pasiones creativas.</p> <p>Nuestra pasión por el arte, la fotografía, la moda nos llevó a crear un espacio integral que una todas estas en un mismo lugar, por eso en Yeah! podés encontrar que conviven varias actividades y profesiones a la vez.</p> <p>El staff de Yeah! cuenta con varios profesionales, reconocidos, con años de experiencia y trayectoria… querés conocernos?</p>
eos
cursos_body = <<-eos
<p>Nuestros talleres son intensivos. Tenemos talleres de un d&iacute;a y dos, seg&uacute;n los contenidos y el programa. Tambi&eacute;n tenemos talleres particulares e individuales que podes hacerlos cuando quieras, el tiempo que quieras:</p>\r\n\r\n<p>Los talleres son pensando tantos para principiantes y avanzados.</p>\r\n\r\n<p>PEDI MAS INFO DE CADA TALLER:</p>\r\n\r\n<p>- TALLER INTENSIVO DE RECIEN NACIDOS / NEWBORNS.</p>\r\n\r\n<p>- TALLER INTENSIVO DE FOTOGRAFIA DE EMBARAZADAS.</p>\r\n\r\n<p>- TALLER INTENSIVO DE UN D&Iacute;A DE ILUMINACI&Oacute;N.</p>\r\n\r\n<p>- TALLER INTENSIVO DE FOTOPERIODISMO DE BODAS.</p>\r\n\r\n<p>- TALLER INTENSIVO DE FOTOGRAFIA DE ALIMENTO / FOOD STYLING.</p>\r\n\r\n<p>- SEMINARIO INTENSIVO DE MARKETING PARA FOTOGRAFOS.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>CLASES INTENSIVAS Y PARTICULARES E INDIVIDUALES DE:</p>\r\n\r\n<p>- FOTOGRAFIA B&Aacute;SICA.</p>\r\n\r\n<p>- PHOTOSHOP.</p>\r\n\r\n<p>- MARKETING PARA FOTOGRAFOS.</p>\r\n\r\n<p>Manda un mail a <a href=\"mailto:info@yeahespacio.com.ar\">info@yeahespacio.com.ar</a> y ped&iacute; la info sobre el taller que te gustar&iacute;a hacer, nosotros te enviamos la info completa (programa, duraci&oacute;n, costo, d&iacute;as, etc.)</p>\r\n
eos
espacio_body = <<-eos
<p>Tenemos los equipos que necesit&aacute;s para realizar tus producciones fotogr&aacute;ficas, para books, campa&ntilde;as, editorial, look book, etc.&nbsp;&nbsp;</p>\r\n\r\n<p>Podes alquilar nuestro espacio, para tus producciones, para info de valores, disponibilidad y mas detalle sobre los equipos con los que trabajamos, escribinos a <a href=\"mailto:info@yeahespacio.com\">info@yeahespacio.com</a>, tambi&eacute;n podes llamar al 20 75 91 37 o por whatsapp al 15 56 58 00 68.</p>\r\n
eos
nosotros_body = <<-eos
<p>Yeah! es un equipo formado por diferentes&nbsp;fot&oacute;grafos, dise&ntilde;adores y&nbsp;publicistas que unen su amor al arte, la ense&ntilde;anza, la moda y la&nbsp;fotograf&iacute;a.</p>\r\n\r\n<p>Ac&aacute;&nbsp;podes conocer el trabajo particular de&nbsp;cada uno de ellos:</p>\r\n\r\n<p><a href=\"http://www.verochfotografa.com/\">www.verochfotografa.com</a></p>\r\n\r\n<p><a href=\"http://phsacch.tumblr.com/\">http://phsacch.tumblr.com</a></p>\r\n\r\n<p><a href=\"http://www.cynthyavs.com/\">http://www.cynthyavs.com</a></p>\r\n\r\n<p>Tambi&eacute;n pasaron por el estudio dictando clases y aportando un granito de arena:</p>\r\n\r\n<p><a href=\"http://rconcept.com.ar/\">http://rconcept.com.ar</a></p>\r\n\r\n<p><a href=\"http://julietaromalde.com.ar/\">http://julietaromalde.com.ar</a></p>\r\n\r\n<p><a href=\"http://alejandraroviraruiz.tumblr.com/\">http://alejandraroviraruiz.tumblr.com</a></p>\r\n\r\n<p><a href=\"http://adrianstehlik.com/\">http://adrianstehlik.com</a></p>\r\n\r\n<p><a href=\"http://www.gustavocampos.net/\">http://www.gustavocampos.net</a></p>\r\n
eos

contact_body = <<-eos
<div> <div>Si tenes dudas, consultas sobre talleres o disponibilidad del estudio:</div> <div>&nbsp;</div> <div>NO DUDES EN ESCRIBIRNOS o LLAMARNOS a</div> <div><a href=\"mailto:info@yeahespacio.com.ar\">info@yeahespacio.com.ar</a></div> <div>Tel Fijo 20 75 91 37</div> <div>WhatsApp 15 56 58 00 68</div> <div>&nbsp;</div> <div>Podes buscarnos en Facebook en Yeah Espacio Creativo y También podes seguirnos en Instagram yeahfotoestudio y Twitter @yeahfoto</div> <div>&nbsp;</div> <div>&nbsp;</div> <div>Esperamos tu contacto!!</div> </div>
eos

Page.create([
  {title: 'home', body: home_body, active: true, position: 1},
  {title: 'nosotros', body: nosotros_body, active: true, position: 2},
  {title: 'cursos', body: cursos_body, active: true, position: 3},
  {title: 'nuestro espacio', body: espacio_body, active: true, position: 4},
  {title: 'contacto', body: contact_body, active: true, position: 5},
])

User.create([
  {name: 'vero', email: 'yeah.espacio@gmail.com', password: 'cambiameporfa'},
  {name: 'ivan', email: 'ivan6258@gmail.com', password: 'cambiameporfa'}
])
