class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title, null: false
      t.string :slug, null: false
      t.boolean :active, default: true
      t.integer :position
      t.text :body

      t.timestamps null: false
    end
    add_index :pages, :slug, unique: true
  end
end
