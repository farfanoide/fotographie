Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  resources :pages
  root 'pages#default_page'
  get '/contacto' => 'contact#index', as: 'contact'
  post '/contacto' => 'contact#deliver', as: 'send_message'
  get '/nuestro_trabajo' => 'albums#work', as: 'our_work'
  get '/album/:id' => 'albums#show', as: 'album'
  get '/admin' => 'admin#index', as: 'admin'

  get 'sort_pages' => 'pages#sort', as: 'sort_pages'
  post 'sort_pages' => 'pages#apply_sort', as: 'apply_sort'
end
