# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
Rails.application.config.assets.paths += Dir[Rails.root + 'bower_components/*']
Rails.application.config.assets.paths += Dir[Rails.root + 'app/assets/fonts/*']
Rails.application.config.assets.paths << Dir[Rails.root + 'bower_components/fancybox/source'].first

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( admin.js *.woff *.ttf *.svg *.eot ckeditor/* )
