source 'https://rubygems.org'

gem 'rails'   # Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'sqlite3' # Use sqlite3 as the database for Active Record
gem 'pg'

gem 'flickraw'    # Flicker API Client
gem 'devise'      # User Authentication
gem 'ckeditor'    # WYSIWYG Editor
gem 'carrierwave' # Uploads
gem 'mini_magick' # Image Manipulation
gem 'simple_form' # Easy Forms
gem 'dotenv-rails'
gem 'pony'         # Mail stuff because activemailer is full of shit

# Templating Engine
gem 'slim'

gem 'sass-rails'   # Use SCSS for stylesheets
gem 'uglifier'     # Use Uglifier as compressor for JavaScript assets
gem 'jquery-rails' # Use jquery as the JavaScript library
gem 'turbolinks'   # Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'bower'        # Asset manager
gem 'nav_lynx'
gem 'jquery-ui-rails'

group :development, :test do
  # gem 'byebug'      # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  # gem 'web-console' # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'spring'     # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'pry-rails'  # Much better than irb
  gem 'pry-doc'
  gem 'capistrano' # Deploy like a pro
  gem 'thin'       # Use Thin instead of Webrick

  # Live Reload
  gem 'guard', require: false
  gem 'rb-inotify', require: false # event lib for linux
  gem 'rb-fsevent', require: false # event lib for mac
  gem 'rack-livereload'
  gem 'guard-livereload', require: false
end

group :production do
  gem 'rails_12factor'
end
